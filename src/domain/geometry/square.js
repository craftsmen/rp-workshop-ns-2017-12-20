const { almostEqual } = require('../../util/utils');

module.exports = class Square {

	constructor(name, size) {
		this.name = name;
		this.size = size;
	}

	calculateArea() {
		return this.size * this.size;
	}

	calculateCircumference() {
		return 4 * this.size;
	}

	getName() {
		return this.name;
	}

	equals(other) {
		return (
			other instanceof Square &&
			this.name === other.name &&
			almostEqual(this.size, other.size)
		);
	}

	toString() {
		return `Square(name='${this.name}', size=${this.size})`;
	}
}
