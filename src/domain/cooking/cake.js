const _ = require('lodash');

module.exports = class Cake {

	constructor(name, ingredients = [], baked = false) {
		this.name = name;
		this.ingredients = ingredients;
		this.baked = baked;
	}

	getName() {
		return this.name;
	}

	addIngredient(ingredient) {
		if (this.baked) {
			throw 'Nope. You cannot add any ingredients after the cake has been baked.';
		}
		return new Cake(this.name, [...this.ingredients, ingredient], this.baked);
	}

	bake() {
		if (!this.ingredients.length) {
			throw 'Trying to serve "gebakken lucht"? Add some ingredients before baking the cake!';
		}
		if (this.baked) {
			throw 'Oops, you\'ve baked the cake again, now it is burned!';
		}
		return new Cake(this.name, this.ingredients, true);
	}

	toString() {
		return [
			this.name + ':',
			...this.ingredients.map((ingredient) => ingredient.getName().toLowerCase()),
			this.baked ? '- freshly baked, om nom nom!' : '- still uncooked :('
		].join(' ');
	}

	equals(other) {
		return (
			!!other &&
			setEquals(this.ingredients, other.ingredients) &&
			this.baked === other.baked
		);
	}
}

function setEquals(first, second) {
	return (
		_.isArray(first) &&
		_.isArray(second) &&
		first.length === second.length &&
		_.intersectionWith(first, second, objectEquals).length === first.length
	);
}

function objectEquals(first, second) {
	if (typeof first.equals === 'function') {
		return first.equals(second);
	}
	return first === second;
}
