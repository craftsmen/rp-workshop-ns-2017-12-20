module.exports = class Ingredient {

	constructor(name, type) {
		this.name = name;
		this.type = type;
	}

	getName() {
		return this.name;
	}

	getType() {
		return this.type;
	}

	equals(other) {
		return (!!other) && this.name === other.name && this.type === other.type;
	}
}